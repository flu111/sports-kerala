export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'fa fa-dashboard',
      badge: {
        variant:'success',
        text: 'NEW'
      }
    },
    {
      title: true,
      name: 'UI elements',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },

    {
      name: 'BOOKING',
      url: '/components',
      icon: 'fa fa-puzzle-piece',
      children: [
        { 
          name: 'AUDITORIUM',
          url: '/components/buttons',
          icon: 'fa fa-puzzle-piece',
          badge: {
            variant: 'danger',
            text: 'NEW'
          }
        },
        {
          name: 'SWIMMING',
          url: '/components/badges',
          icon: 'fa fa-id-badge'
        },
        {
          name: 'FOOTBALL',
          url: '/components/alerts',
          icon: 'fa fa-exclamation-triangle'
        },
        {
          name: 'BASKETBALL',
          url: '/components/progressbars',
          icon: 'fa fa-spinner'
        },
        {
          name: 'CRICKET',
          url: '/components/grids',
          icon: 'fa fa-th'
        },
        {
          name: 'VOLLYBALL',
          url: '/components/typography',
          icon: 'fa fa-file-word-o'
        }
      ]
    },
    {
      name: 'Maps',
      url: '/components/maps/',
      icon: 'fa fa-map-marker',
      badge: {
        variant: 'info',
        text: 'NEW'
      },
      children: [
                  {
                    name: 'Google Maps',
                    url: '/components/maps/google-maps',
                    icon: 'fa fa-map-o'
                  },
                  {
                    name: 'Leaflet Maps',
                    url: '/components/maps/leaflet-maps',
                    icon: 'fa fa-street-view'
                  }
      ]
    },
    {
      divider: true
    },
    {
      title: true,
      name: 'Extras'
    },
    {
      name: 'SIGN IN',
      url: '/auth',
      icon: 'fa fa-paperclip',
      children: [
        {
          name: 'Login',
          url: '/auth/login',
          icon: 'fa fa-sign-in'
        },
        {
          name: 'Register',
          url: '/auth/register',
          icon: 'fa fa-sign-in'
        },
        
        
      ]
    },
    
    
    
  ]
}
